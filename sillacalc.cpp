#include <iostream>
#include <map>
#include <vector>
#include <cstdlib>

using namespace std;

int main (int argc, char** argv) {
/*
$ sillacalc A=4 B=9 C=0 D=7 E=1 F=8 G=9 K=5 L=7 P=7 Q=2 T=2 V=6 W=10 X=4 Y=2 Z=4 H=-3 I=-2 J=-2 M=-2 N=-5 O=-2 S=-4 U=-1 R=10
(input)abcd
(output)20
*/
	string word;
	vector<string> v(argv+1, argv+27);
	map<char, int> lettervalues;
	int value;
	for (int i = 0; i < 26; i++) lettervalues[tolower(v[i][0])] = atoi((v[i].substr(2, -1)).c_str());
	while (cin >> word) {
		value = 0;
		for (int i = 0; i < word.size(); i++) value += lettervalues[word[i]];
		cout << value << " " << word << endl;
	}
	return 0;
}
