#!/bin/bash

grep $1 /usr/bin/izastuff/anagram-dict/italiano | grep -E "^.{1,$2}$" | sillacalc ${*:3} | sort -nr | head